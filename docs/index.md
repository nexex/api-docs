<h1><b>Get started</b></h1>

Requests content type: `application/json`

Response content type: `application/json`

To start using exchange API you need to create an account and request for a service token for your account from developers team.
