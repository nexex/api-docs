<h1><b>Web-sockets API</b></h1>

Exchange uses [Pusher](https://pusher.com/) channels to allow easier web-sockets communication. You can get pusher client [libraries](https://pusher.com/docs/libraries) and [docs](https://pusher.com/docs/client_api_guide) on pusher web-site.

<h2>Open connection</h2>

To get data about pusher connection client should send `ws/info` request.

<h3>Pusher client params request</h3>

This request will return data for creating Pusher connection.

Type: `GET`

Endpoint: `api/v1/ws/info`

Authorization: not required

Response:

Status code: `200`

Body:

```json
{
    "ws_host": "ws-st.dlex.io",
    "ws_port": "80",
    "wss_port": "443",
    "app_key": "66F8FBB57F5262368B57A47EE5547",
    "encrypted": "true"
}
```

<h3>Pusher auth params</h3>

Returns auth params for connection to private channel.

Type: `POST`

Endpoint: `api/v1/ws/auth`

Authorization: required

Body:

```json
{
  "socket_id": "sdfsdf",
  "channel": "some_channel"
}
```

+ `socket_id` - Socket id from client
+ `channel` - Channel name

Response:

Status code: `200`

Body:

```json
{
    "auth": "66F8FBB57F5262368B57A47EE5547:c0c31c0525aa32bbbd76d8a98fb0532b6b13407a21cabc791fc34d90c2dcb414"
}
```

Possible errors:

+ `403` - user is forbidden to access this channel

<h2>Channels</h2>

<h3>Global</h3>

Name: `market-global`

Events for all markets. Public channel.

<h5>Tickers</h5>

Event name: `tickers`

Event for global tickers change

Data:

```json
{
  "ethbtc": {
    "name": "ETH/BTC",
    "base_unit": "eth",
    "quote_unit": "btc",
    "at": 1536940703,
    "ticker": {
      "low": "0.0072",
      "high": "0.0318",
      "last": "0.0072",
      "open": "0.0318",
      "vol": "1.059",
      "price": "0.0085928",
      "sell": "0.0325",
      "buy": "0.0071"
    }
  }
}
```

+ `last` - price of last trade
+ `vol` - volume per last 24 hours
+ `price` - price per last 24 hours - volume * average_price

<h3>Specific market</h3>

Name: `market-#{market_code}-global`

Channel for specific market. For example, `market-ethbtc-global` channel will track updates on ETH/BTC market. Public channel.

<h5>Orderbook update</h5>

Event name: `update`

Orderbook has been updated. Returns full orderbook for specific market.

*Note: in data first number is price and second is volume*

Data:

```json
{
  "asks": [
    ["0.1212","2.0"],
    ["0.1213","5.52748"]
  ],
  "bids": [
    ["0.0824","0.516211"],
    ["0.082","0.964822"]
  ]
}
```

<h5>New trade</h5>

Event name: `trades`

New trade appeared on market.

```json
{
  "tid": 23545,
  "type": "sell",
  "date": 213443153453,
  "price": "0.1212",
  "amount": "2.0"
}
```

+ `type` - `sell` or `buy`
+ `date` - trade creation date, timestamp in milliseconds

<h3>User channel</h3>

Name: `private-#{user_uuid}`

Private channel for user specific notifications.

<h5>New trade</h5>

Event name: `trade`

New trade appeared.

Data:

```json
{
  "id": 12323412,
  "type": "sell",
  "kind": "ask",
  "at": 213443153453,
  "price": "0.1212",
  "volume": "2.0",
  "market": "ethbtc"
}
```

+ `type` - `sell` or `buy`
+ `kind` - `ask` or `bid`
+ `at` - trade creation date, timestamp in milliseconds

<h5>Balance updates</h5>

Event name: `account`

Account balance updates

Data:

```json
{
  "balance": "2.23435322",
  "locked": "0.0",
  "currecy": "btc"
}
```

<h5>Order updates</h5>

Event name: `order`

Order state changed or fulfillment changed.

Data:

```json
{
  "id": 199983,
  "at": 1536869835,
  "market": "bchbtc",
  "kind": "bid",
  "price": "0.1212",
  "state": "done",
  "volume": "0.0",
  "origin_volume": "0.78154",
  "fulfillment": "1.0"
}
```

+ `at` - order creation date, timestamp in milliseconds
+ `kind` - `ask` or `bid`
+ `state` - `done` for fully executed orders, `wait` for orders in orderbook, `cancel` for canceled orders.
+ `volume` - current volume placed in orderbook
+ `original_volume` - volume with wich order was created
+ `fulfillment` - order execution state, `1.0` - fully executed order, `0.5` - 50% of volume was executed.
