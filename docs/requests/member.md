<h1><b>Member</b></h1>

All request needs authorization.

<h2>Get account info</h2>

Type: `GET`

Endpoint: `api/v1/members/me`

Response:

Status code: `200`

Body:

```json
{
    "email": "test@cryp.email",
    "activated": true,
    "terms_accepted": true,
    "accounts": [
        {
            "currency": "btc",
            "balance": "1.4135599886220364",
            "locked": "0.0107",
            "converted": {
                "eur": {
                    "balance": "8392.766166488972957864027024",
                    "locked": "63.529385879812"
                },
                "usd": {
                    "balance": "9812.978989719486460206085916",
                    "locked": "74.279744782783"
                },
                "btc": {
                    "balance": "1.4135599886220364",
                    "locked": "0.0107"
                }
            }
        },
        {
            "currency": "eth",
            "balance": "0.009203202",
            "locked": "0.0",
            "converted": {
                "eur": {
                    "balance": "2.199061063350035856",
                    "locked": "0.0"
                },
                "usd": {
                    "balance": "2.571183276602407596",
                    "locked": "0.0"
                },
                "btc": {
                    "balance": "0.0003706776393687792",
                    "locked": "0.0"
                }
            }
        },
        {
            "currency": "bch",
            "balance": "2.99761291858",
            "locked": "0.01",
            "converted": {
                "eur": {
                    "balance": "1369.1659081381513122072",
                    "locked": "4.5675207084"
                },
                "usd": {
                    "balance": "1600.85435760443112322816",
                    "locked": "5.34043053952"
                },
                "btc": {
                    "balance": "0.231128345756233074856",
                    "locked": "0.000771041332"
                }
            }
        },
        {
            "currency": "ltc",
            "balance": "0.0",
            "locked": "0.0",
            "converted": {
                "eur": {
                    "balance": "0.0",
                    "locked": "0.0"
                },
                "usd": {
                    "balance": "0.0",
                    "locked": "0.0"
                },
                "btc": {
                    "balance": "0.0",
                    "locked": "0.0"
                }
            }
        },
        {
            "currency": "xlm",
            "balance": "0.0",
            "locked": "0.0",
            "converted": {
                "eur": {
                    "balance": "0.0",
                    "locked": "0.0"
                },
                "usd": {
                    "balance": "0.0",
                    "locked": "0.0"
                },
                "btc": {
                    "balance": "0.0",
                    "locked": "0.0"
                }
            }
        },
        {
            "currency": "trx",
            "balance": "0.0",
            "locked": "0.0",
            "converted": {
                "eur": {
                    "balance": "0.0",
                    "locked": "0.0"
                },
                "usd": {
                    "balance": "0.0",
                    "locked": "0.0"
                },
                "btc": {
                    "balance": "0.0",
                    "locked": "0.0"
                }
            }
        },
        {
            "currency": "nex",
            "balance": "131.0",
            "locked": "0.0",
            "converted": {
                "eur": {
                    "balance": "0.0",
                    "locked": "0.0"
                },
                "usd": {
                    "balance": "0.0",
                    "locked": "0.0"
                },
                "btc": {
                    "balance": "0.0",
                    "locked": "0.0"
                }
            }
        }
    ],
    "roles": [
        "super_admin"
    ],
    "uuid": "19e5e3742213-4007-981b-004e41752e28",
    "kyc_tier": {
        "code": "tier_0",
        "name": "Tier 0"
    },
    "total_balance": {
        "btc": "2.7645095",
        "usd": "18079.41",
        "eur": "15551.43"
    }
}
```
