<h1><b>Markets</b></h1>

<h2>Get markets</h2>

Request doesn't require auth.

Type: `GET`

Endpoint: `api/v1/markets`

Response:

Status code: `200`

Body:

```json
[
    {
        "id": "ethbtc",
        "name": "ETH/BTC"
    }
]
```
