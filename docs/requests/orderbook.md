<h1><b>Orderbook</b></h1>

Contains only public requests

<h2>Get orderbook</h2>

Type: `GET`

Endpoint: `api/v1/order_book`

Query params:

+ `market` - market code. **Required**
+ `asks_limit` - limit the number of returned sell orders. Default is 20. Should be in between 1 and 200.
+ `bids_limit` - limit the number of returned buy orders. Default is 20. Should be in between 1 and 200.

Response:

Status: `200`

Body:

```json
{
    "asks": [
        {
            "id": 199576,
            "side": "sell",
            "ord_type": "limit",
            "price": "0.1119",
            "avg_price": "0.0",
            "state": "wait",
            "market": "bchbtc",
            "created_at": "2018-08-27T12:18:52Z",
            "volume": "0.01",
            "remaining_volume": "0.01",
            "executed_volume": "0.0",
            "fulfillment": "0.0",
            "trades_count": 0
        }
    ],
    "bids": [
        {
            "id": 196553,
            "side": "buy",
            "ord_type": "limit",
            "price": "0.1119",
            "avg_price": "0.1119",
            "state": "wait",
            "market": "bchbtc",
            "created_at": "2018-07-16T00:55:56Z",
            "volume": "10.748167",
            "remaining_volume": "0.376116",
            "executed_volume": "10.372051",
            "fulfillment": "0.965006498317340994050427389",
            "trades_count": 18
        }
    ]
}
```
