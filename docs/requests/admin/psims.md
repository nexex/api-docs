<h1><b>PSIMs</b></h1>

PSIM - Payment System Integration Module. PSIM it is external service that represents logic of working with some specific system. PSIM manages deposits and withdraws for some specific payment system. Particular case of PSIM is cryptocurrency wallet. PSIM may support some particular assets like BTC or even asset types like ERC20.

<h2>Get PSIMs list</h2>

Type: `GET`

Endpoint: `api/v1/admin/psims`

Pagination: `true`

Auth: Super admin

Response body:

```json
{
  "items": [
    {
      "id": "dff238cfbe7d-4968-9266-a909b90ee958",
      "supported_assets": [
        "bch",
        "btc",
        "btg",
        "dash",
        "ltc"
      ],
      "supported_asset_types": [
        "erc20"
      ],
      "endpoint": "http://btc.psim:14500",
      "is_connected": true
    }
  ]
}
```

<h2>Get PSIM</h2>

Type: `GET`

Endpoint: `api/v1/admin/psims/{uuid}`

Auth: Super admin

Params:

- `config` - `bool`, represents if config data should be included into response.
- `balances` - `bool`, represents if balances data should be included into response.

Response body:

```json
{
  "id": "dff238cfbe7d-4968-9266-a909b90ee958",
  "supported_assets": [
    "bch",
    "btc",
    "btg",
    "dash",
    "ltc"
  ],
  "supported_asset_types": [
    "erc20"
  ],
  "endpoint": "http://localhost:14500",
  "is_connected": true,
  "config": {
    "type": "crypto",
    "data": {
      "hot_address": "1x6YnuBVeeE65dQRZztRWgUPwyBjHCA5g",
      "cold_address": "3H5YUw5MCYMyfjdzv1BmDcjtrqUBSok3e4",
      "required_confirmations": 3
    }
  },
  "balances": [
    {
      "asset": "bch",
      "hot": "10.0",
      "cold": "90.0"
    }
  ]
}
```

where:

- `config` - represents some details about PSIM configuration. Details is placed in `data` field and details structure and editable fields depends on `type` field.

  Possible variants:

  Type: `crypto`

  ```json
  {
    "hot_address": "1x6YnuBVeeE65dQRZztRWgUPwyBjHCA5g",
    "cold_address": "3H5YUw5MCYMyfjdzv1BmDcjtrqUBSok3e4",
    "required_confirmations": 3
  }
  ```

  where `cold_address` and `required_confiramtions` is editable.

- `balances` - represents list of balances of currencies connected to PSIM. `cold` - may be not presented.

*Request will be slower with including additional params, like config and balances*

<h2>Get balances</h2>

Type: `GET`

Endpoint: `api/v1/admin/psims/{uuid}/balances`

Auth: Super admin

Response body:

```json
[
  {
    "asset": "bch",
    "hot": "10.0",
    "cold": "90.0"
  }
]
```

<h2>Get config</h2>

Type: `GET`

Endpoint: `api/v1/admin/psims/{uuid}/config`

Auth: Super admin

Response body:

```json
{
  "type": "crypto",
  "data": {
    "hot_address": "1x6YnuBVeeE65dQRZztRWgUPwyBjHCA5g",
    "cold_address": "3H5YUw5MCYMyfjdzv1BmDcjtrqUBSok3e4",
    "required_confiramtions": 3
  }
}
```

<h2>Update config</h2>

Type: `PATCH`

Endpoint: `api/v1/admin/psims/{uuid}/config`

Auth: Super admin

Request body:

```json
{
  "cold_address": "3H5YUw5MCYMyfjdzv1BmDcjtrqUBSok3e4",
  "required_confiramtions": 3
}
```

Response body:

```json
{
  "type": "crypto",
  "data": {
    "hot_address": "1x6YnuBVeeE65dQRZztRWgUPwyBjHCA5g",
    "cold_address": "3H5YUw5MCYMyfjdzv1BmDcjtrqUBSok3e4",
    "required_confiramtions": 3
  }
}
```
