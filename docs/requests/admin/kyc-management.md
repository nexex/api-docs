<h1><b>KYC management</b></h1>

<h2>Change user KYC tier</h2>

Type: `POST`

Endpoint: `/api/v1/admin/members/tier`

Auth: Customers admin

Request body:

```json
{
  "uuid": "d939c88a1b8b-4e10-b7bf-83ad3095477f",
  "kyc_tier_code": "tier_1"
}
```
