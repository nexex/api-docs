<h1><b>Assets</b></h1>

<h2>Model</h2>

```json
{
  "code": "bch",
  "name": "Bitcoin Cash",
  "precision": 8,
  "available_actions": 524287,
  "options": {
    "tx_url": "https://testnet.blockexplorer.com/tx/${value}",
    "address_url": "https://testnet.blockexplorer.com/address/${value}"
  },
  "type": "crypto",
  "psim_id": "dff238cfbe7d-4968-9266-a909b90ee958",
  "created_at": "2018-10-06T13:37:03.000Z",
  "updated_at": "2018-10-06T13:37:03.000Z"
}
```

where:

- `available_actions` - bit mask for actions available with asset.

 Deposit - `0b01`

 Withdraw - `0b10`

- `type` - determines type of currency. Possible values: `crypto`, `fiat`, `erc20`.
- `options` - represents asset details. The structure of options depends on asset type.

 Possible values:

 Type: `crypto`

```json
{
 "tx_url": "https://testnet.blockexplorer.com/tx/${value}",
 "address_url": "https://testnet.blockexplorer.com/address/${value}"
}
```

 Type: `erc20`

```json
{
 "tx_url": "https://testnet.blockexplorer.com/tx/${value}",
 "address_url": "https://testnet.blockexplorer.com/address/${value}",
 "contract_address": "0xb8c77482e45f1f44de1745f52c74426c631bdd52"
}
```

 <h2>Requests</h2>

 <h3>Get assets list</h3>

 Get assets list supported by exchange.

 Type: `GET`

 Endpoint: `api/v1/admin/assets`

 Pagination: `true`

 Auth: Super admin

 <h3>Get asset</h3>

 Type: `GET`

 Endpoint: `api/v1/admin/assets/{code}`

 Auth: Super admin

 <h3>Create asset</h3>

 Type: `POST`

 Endpoint: `api/v1/admin/assets`

 Auth: Super admin

 Request body:

 ```json
 {
   "code": "bnb",
   "name": "Binance Token",
   "precision": 8,
   "type": "erc20",
   "available_actions": 23543534,
   "options": {
     "tx_url": "https://testnet.blockexplorer.com/tx/${value}",
     "address_url": "https://testnet.blockexplorer.com/address/${value}",
     "contract_address": "0xb8c77482e45f1f44de1745f52c74426c631bdd52"
   }
 }
 ```

 where:

 - `code` - **Required**
 - `name` - **Required**
 - `precision` - **Required**
 - `type` - **Required**

<h3>Update asset</h3>

Type: `PATCH`

Endpoint: `api/v1/admin/assets`

Auth: Super admin

Request body:

```json
{
  "name": "Binance Token",
  "precision": 8,
  "available_actions": 23543534,
  "options": {
    "tx_url": "https://testnet.blockexplorer.com/tx/${value}",
    "address_url": "https://testnet.blockexplorer.com/address/${value}",
    "contract_address": "0xb8c77482e45f1f44de1745f52c74426c631bdd52"
  }
}
```

*At list one of fields should be presented*
