<h1><b>Deposit requests</b></h1>

Represents request to deposit some asset. Exists to distinguish different deposits.

<h2>Model</h2>

```json
{
    "uuid": "9faeec839fa4-46d6-97fe-ff83335399c9",
    "asset": "btc",
    "expires_at": null,
    "options": {
        "type": "address",
        "data": {
            "address": "94fc1bf619e843648cdc9ca5c64a4fa6"
        }
    }
}
```

where

- `options` - contains details about deposit request. Details is placed in `data` field and details structure depends on `type` field.

  Possible values:

Type: `address`

```json
{
 "address": "94fc1bf619e843648cdc9ca5c64a4fa6"
}
```

Type: `address_with_payload`

```json
{
 "address": "94fc1bf619e843648cdc9ca5c64a4fa6",
 "payload": "2343124"
}
```

Type: `sepa`

```json
{
  "iban": "MT46EMON02015001002000100000120",
  "bic": "EMONMTM2",
  "payment_reference": "13773602039654423166",
  "account_name": "Nexex (conto Tecnico)",
  "address": "REGENT HOUSE FLAT 45 BISAZZA STREET, SLIEMA, SLM 1640",
  "bank_name": "EM@NEY PLC"
}
```

<h2>Requests</h2>

<h3>Get deposit requests list</h3>

Type: `GET`

Endpoint: `api/v1/deposit-requests`

Pagination: `true`

Auth: Registered user

Params:

- `asset` - asset code to filter values.

<h3>Get deposit request</h3>

Type: `GET`

Endpoint: `api/v1/deposit-requests/{uuid}`

Auth: Registered user

<h3>Get current deposit request</h3>

Gets current user deposit request for specified asset. If user doesn't have any deposit requests for this asset, new deposit request will be generated.

Type: `GET`

Endpoint: `api/v1/deposit-requests/current`

Auth: Registered user

Params:

- `asset` - asset code to filter values. **Required**


<h3>Create deposit request</h3>

Type: `POST`

Endpoint: `api/v1/deposit-requests`

Auth: Registered user

Body:

```json
{
  "asset": "btc"
}
```

where:

- `asset` - **Reqeired**
