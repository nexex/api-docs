<h1><b>Withdrawals</b></h1>

<h2>Model</h2>

```json
{
  "uuid": "2c62f47e8d63-431d-b460-7ee56b98c238",
  "asset": "btc",
  "amount": "0.01",
  "fee": "0.0",
  "status": "created",
  "options": {
    "type": "crypto",
    "data": {
      "tx_id": "fdfbf222888edc45ac37576a52b05c406a411ca1d26f936ea6854e0908b581ac",
      "counterparty_address": "1J8rcVPwRjJdi1jhk95AsjMfsLBbP7x7pS"
    }
  },
  "done_at": null,
  "updated_at": "2018-10-08T11:43:08.000Z",
  "created_at": "2018-10-08T11:43:08.000Z",
  "destination": "1J8rcVPwRjJdi1jhk95AsjMfsLBbP7x7pS"
}
```

where

- `destination` is asset specific, for crypto assets it contains address for fiat it contains IBAN and BIC formated in such way `IBAN:BIC`, for example, for IBAN: `MT46EMON02015001002000100000120` and BIC: `EMONMTM2` it will be `MT46EMON02015001002000100000120:EMONMTM2`.
- `options` - is additional details. Details is placed in `data` field and details structure depends on `type` field.

  Possible values:

Type: `crypto`

```json
{
  "tx_id": "fdfbf222888edc45ac37576a52b05c406a411ca1d26f936ea6854e0908b581ac",
  "counterparty_address": "1MkMrYCidKhNJjWpHiqrZPTHjih5mdWdn2"
}
```

Type: `sepa`

```json
{
  "counterparty_iban": "MT46EMON02015001002000100000120",
  "counterparty_bic": "EMONMTM2"
}
```

<h2>Requests</h2>

<h3>Get list</h3>

Type: `GET`

Endpoint: `api/v1/withdrawals`

Pagination: `true`

Auth: Registered user

Params:

- `asset` - asset code to filter values.

<h3>Get withdrawal</h3>

Type: `GET`

Endpoint: `api/v1/withdrawals/{uuid}`

Auth: Registered user

<h3>Create withdrawal</h3>

Type: `POST`

Endpoint: `api/v1/withdrawals`

Auth: Registered user

Body:

```json
{
  "asset": "btc",
  "amount": "10.0",
  "destination": "1MkMrYCidKhNJjWpHiqrZPTHjih5mdWdn2",
  "payment_details": {
    "payload": "123235"
  }
}
```

where:

- `asset` - **Required**
- `amount` - **Required**
- `destination` - **Required**
- `payment_details` - custom payment details that may be proceed by payment system can posted here. *For now, this field can be used only for XLM payments, where client should provide payload to include it as memo to transaction*
