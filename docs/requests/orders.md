<h1><b>Orders</b></h1>

All requests need authorization

<h2>Get orders list</h2>

Type: `GET`

Endpoint: `api/v1/orders`

Query params:

+ `market` - market code, in case of showing orders for all markets use `all`. **Required**
+ `page` - page to load
+ `per_page` - page size, default 10
+ `order_by` - ordering field, possible values: volume, fulfillment
+ `order` - `asc` or `desc`
+ `state` - state of order, possible values `wait`, `done`, `cancel`
+ `type` - `sell` or `buy`
+ `ord_type` - `market` or `limit`

Response:

Status code: `200`

Body:

```json
{
    "order": "price asc",
    "total_count": 1,
    "per_page": 20,
    "total_pages": 1,
    "page": 1,
    "prev_page": null,
    "next_page": null,
    "items": [
        {
            "id": 199576,
            "side": "sell",
            "ord_type": "limit",
            "price": "0.1119",
            "avg_price": "0.0",
            "state": "wait",
            "market": "bchbtc",
            "created_at": "2018-08-27T12:18:52Z",
            "volume": "0.01",
            "remaining_volume": "0.01",
            "executed_volume": "0.0",
            "fulfillment": "0.0",
            "trades_count": 0
        }
    ]
}
```

<h2>Create order</h2>

Type: `POST`

Endpoint: `api/v1/orders`

Body:

```json
{
  "market": "bchbtc",
  "side": "sell",
  "volume": "0.1",
  "price": "1",
  "ord_type": "market"
}
```

+ `market` - market code. **Required**
+ `side` - `sell` or `buy`. **Required**
+ `volume` - the amount user want to sell/buy. **Required**
+ `price` - order price, optional for market order
+ `ord_type` - `market` or `limit`, default `limit`.

Response:

Status code: `201`

Body:

```json
{
    "id": 199576,
    "side": "sell",
    "ord_type": "limit",
    "price": "0.1119",
    "avg_price": "0.0",
    "state": "wait",
    "market": "bchbtc",
    "created_at": "2018-08-27T12:18:52Z",
    "volume": "0.01",
    "remaining_volume": "0.01",
    "executed_volume": "0.0",
    "fulfillment": "0.0",
    "trades_count": 0
}
```

Possible errors:

+ `400`
    - `2002` - failed to create order

<h2>Create multiply orders</h2>

Type: `POST`

Endpoint: `api/v1/orders/multi`

Body:

```json
{
    "orders": [
      {
        "market": "bchbtc",
        "side": "sell",
        "volume": "0.1",
        "price": "1",
        "ord_type": "market"
      }
    ]
}
```

+ `market` - market code. **Required**
+ `side` - `sell` or `buy`. **Required**
+ `volume` - the amount user want to sell/buy. **Required**
+ `price` - order price, optional for market order
+ `ord_type` - `market` or `limit`, default `limit`.

Response:

Status code: `201`

Body:

```json
[
  {
      "id": 199576,
      "side": "sell",
      "ord_type": "limit",
      "price": "0.1119",
      "avg_price": "0.0",
      "state": "wait",
      "market": "bchbtc",
      "created_at": "2018-08-27T12:18:52Z",
      "volume": "0.01",
      "remaining_volume": "0.01",
      "executed_volume": "0.0",
      "fulfillment": "0.0",
      "trades_count": 0
  }
]
```

Possible errors:

+ `400`
    - `2002` - failed to create order

<h2>Cancel all orders</h2>

Type: `POST`

Endpoint: `api/v1/orders/clear`

Query params:

+ `side` - `sell` or `buy`
+ `market` - market code, in case of showing orders for all markets use `all`.

Response:

Status code: `201`

Body:

```json
[
  {
      "id": 199576,
      "side": "sell",
      "ord_type": "limit",
      "price": "0.1119",
      "avg_price": "0.0",
      "state": "wait",
      "market": "bchbtc",
      "created_at": "2018-08-27T12:18:52Z",
      "volume": "0.01",
      "remaining_volume": "0.01",
      "executed_volume": "0.0",
      "fulfillment": "0.0",
      "trades_count": 0
  }
]
```

Possible errors:

+ `400`
     - `2003` - failed to cancel order
