<h1><b>Assets</b></h1>

Asset represents any commodity that can be traded on exchange: fiat, cryptocurrency, erc20 tokens, etc.

<h2>Model</h2>

```json
{
  "code": "btc",
  "name": "Bitcoin",
  "precision": 8,
  "available_actions": 56512,
  "type": "crypto",
  "options": {
    "tx_url": "https://testnet.blockexplorer.com/tx/${value}",
    "address_url": "https://testnet.blockexplorer.com/address/${value}"
  }
}
```

where

- `available_actions` - bit mask for actions available with asset.

  Deposit - `0b01`

  Withdraw - `0b10`

- `type` - determines type of currency. Possible values: `crypto`, `fiat`, `erc20`.
- `options` - represents asset details. The structure of options depends on asset type.

  Possible values:

  Type: `crypto`

```json
{
 "tx_url": "https://testnet.blockexplorer.com/tx/${value}",
 "address_url": "https://testnet.blockexplorer.com/address/${value}"
}
```

  Type: `erc20`

```json
{
 "tx_url": "https://testnet.blockexplorer.com/tx/${value}",
 "address_url": "https://testnet.blockexplorer.com/address/${value}",
 "contract_address": "0xb8c77482e45f1f44de1745f52c74426c631bdd52"
}
```

<h2>Requests</h2>

<h3>Get assets</h3>

Get assets list supported by exchange.

Type: `GET`

Endpoint: `api/v1/assets`

Pagination: `true`

Auth: `false`

<h3>Get asset</h3>

Get asset details

Type: `GET`

Endpoint: `api/v1/assets/{asset_code}`

Auth: `false`
