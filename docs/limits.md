<h1><b>Request limits</b></h1>

Client are limited to make 120 requests per minute using one access key. In case of reaching this limit API will return response with status code `429` and empty body.
